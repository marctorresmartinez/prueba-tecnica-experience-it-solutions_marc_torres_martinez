import { Routes } from '@angular/router';

export const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./user/user.routes').then(m => m.USER_ROUTES)
    },
    {
        path: 'user',
        loadChildren: () => import('./user/user.routes').then(m => m.USER_ROUTES)
    },
    {
        path: 'users',
        loadChildren: () => import('./user/user.routes').then(m => m.USER_ROUTES)
    }
];
