export interface User {
    id: number;
    username: string;
    email: string;
    name: string;
    surname1: string;
    surname2: string;
}