import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root',
})

export class UserService {

  constructor(private http: HttpClient) {}

  // Get users from the mock
  getUsers(): Observable<any> {
    const url = "/assets/mocks/users_mock.json";
    return this.http.get(url);
  }
}