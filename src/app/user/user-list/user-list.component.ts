import { Component } from '@angular/core';
import { User } from '../../core/models/user.interface';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UserService } from '../../core/services/user.service';

@Component({
  selector: 'app-user-list',
  standalone: true,
  imports: [ HttpClientModule, CommonModule, FormsModule ],
  providers: [ UserService ],
  templateUrl: './user-list.component.html',
  styleUrl: './user-list.component.css'
})

export class UserListComponent {

  users: User[] = [];

  filteredUsers: User[] = [];
  
  pages: number = 1;

  usersFilters = {
    "name": "",
    "surname1": "",
    "surname2": "",
    "email": "",
    "page": 1
  }

  constructor(private userService: UserService){}

  ngOnInit(): void {
    this.loadUsers();
  }

  // Get users from the service and load them
  // Also calculate number of pages for the pagination
  loadUsers() {
    this.userService.getUsers().subscribe((response: any) => {
      this.users = response.users;
      this.filteredUsers = response.users;
      this.pages = Math.ceil(response.users.length / 5);
      if(this.pages == 0) this.pages = 1;
    });
  }

  // Filter clients from the inputs
  filter() {
    this.filteredUsers = this.users.filter(user => 
      user.email.toLowerCase().includes(this.usersFilters.email.toLowerCase()) &&
      user.name.toLowerCase().includes(this.usersFilters.name.toLowerCase()) &&
      user.surname1.toLowerCase().includes(this.usersFilters.surname1.toLowerCase()) &&
      user.surname2.toLowerCase().includes(this.usersFilters.surname2.toLowerCase())
    )

    this.pages = Math.ceil(this.filteredUsers.length / 5);
    if(this.pages == 0) this.pages = 1;

    this.usersFilters.page = 1;
  }

  // Change page in the pagination when clicked
  changePage(page: number) {
    this.usersFilters.page = page;
  }
}
